terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.15.1"
}

resource "aws_instance" "app_server" {
  ami           = "ami-09e6ddddd7e426f25ce0d71"
  instance_type = "t2.micro"

  tags = {
    Owner = "Shrikant Dhanawade",
    Name  = "Test VM QA TEST"
  }
}
